<?php
defined('_JEXEC') or die;

JLoader::import('components.com_fields.libraries.fieldsplugin', JPATH_ADMINISTRATOR);


class PlgFieldsSlides extends FieldsPlugin
{
        public function onCustomFieldsPrepareDom($field, DOMElement $parent, JForm $form) {

            $fieldNode = parent::onCustomFieldsPrepareDom($field, $parent, $form);

            if (!$fieldNode) {
                return $fieldNode;
            }

            $fieldNode->setAttribute('type', 'subform');
            $fieldNode->setAttribute('formsource', "plugins/fields/slides/subform.xml");
            $fieldNode->setAttribute('multiple', 'true');
            $fieldNode->setAttribute('layout', 'joomla.form.field.subform.repeatable-table');


            return $fieldNode;
        }

}
